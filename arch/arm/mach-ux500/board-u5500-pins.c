/*
 * Copyright (C) ST-Ericsson SA 2011
 *
 * License terms: GNU General Public License (GPL) version 2
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/gpio/nomadik.h>
#include <plat/pincfg.h>

#include "pins-db5500.h"
#include "pins.h"

static pin_cfg_t u5500_pins_default[] = {
	/* Keypad */
	GPIO128_KP_I0	| PIN_INPUT_PULLUP,
	GPIO130_KP_I1	| PIN_INPUT_PULLUP,
	GPIO132_KP_I2	| PIN_INPUT_PULLUP,
	GPIO134_KP_I3	| PIN_INPUT_PULLUP,
	GPIO137_KP_O4	| PIN_INPUT_PULLUP,
	GPIO139_KP_O5	| PIN_INPUT_PULLUP,

	/* MSP */
	GPIO32_MSP0_TCK		| PIN_INPUT_PULLDOWN,
	GPIO33_MSP0_TFS		| PIN_INPUT_PULLDOWN,
	GPIO34_MSP0_TXD		| PIN_INPUT_PULLDOWN,
	GPIO35_MSP0_RXD		| PIN_INPUT_PULLDOWN,
	GPIO96_MSP1_TCK		| PIN_INPUT_PULLDOWN,
	GPIO97_MSP1_TFS		| PIN_INPUT_PULLDOWN,
	GPIO98_MSP1_TXD		| PIN_INPUT_PULLDOWN,
	GPIO99_MSP1_RXD		| PIN_INPUT_PULLDOWN,
	GPIO220_MSP2_TCK	| PIN_OUTPUT_LOW,
	GPIO221_MSP2_TFS	| PIN_OUTPUT_LOW,
	GPIO222_MSP2_TXD	| PIN_OUTPUT_LOW,

	/* DISPLAY_ENABLE */
	GPIO226_GPIO        | PIN_OUTPUT_LOW,

	/* Backlight Enable */
	GPIO224_GPIO        | PIN_OUTPUT_HIGH,

	/* MMC0 (POP eMMC) */
	GPIO5_MC0_DAT0		| PIN_INPUT_PULLUP,
	GPIO6_MC0_DAT1		| PIN_INPUT_PULLUP,
	GPIO7_MC0_DAT2		| PIN_INPUT_PULLUP,
	GPIO8_MC0_DAT3		| PIN_INPUT_PULLUP,
	GPIO9_MC0_DAT4		| PIN_INPUT_PULLUP,
	GPIO10_MC0_DAT5		| PIN_INPUT_PULLUP,
	GPIO11_MC0_DAT6		| PIN_INPUT_PULLUP,
	GPIO12_MC0_DAT7		| PIN_INPUT_PULLUP,
	GPIO13_MC0_CMD		| PIN_INPUT_PULLUP,
	GPIO14_MC0_CLK		| PIN_OUTPUT_LOW,

	/* UART3 */
	GPIO165_U3_RXD		| PIN_INPUT_PULLUP,
	GPIO166_U3_TXD		| PIN_OUTPUT_HIGH,
	GPIO167_U3_RTSn		| PIN_OUTPUT_HIGH,
	GPIO168_U3_CTSn		| PIN_INPUT_PULLUP,

	/* AB5500 */
	GPIO78_IRQn             | PIN_SLPM_NOCHANGE,
	GPIO100_I2C0_SCL        | PIN_INPUT_PULLUP | PIN_SLPM_NOCHANGE,
	GPIO101_I2C0_SDA        | PIN_SLPM_NOCHANGE,

	/* TOUCH_IRQ */
	GPIO179_GPIO	| PIN_INPUT_PULLUP,

	/* SDI1 (SD-CARD) */
	GPIO191_MC1_DAT0	| PIN_INPUT_PULLUP,
	GPIO192_MC1_DAT1	| PIN_INPUT_PULLUP,
	GPIO193_MC1_DAT2	| PIN_INPUT_PULLUP,
	GPIO194_MC1_DAT3	| PIN_INPUT_PULLUP,
	GPIO195_MC1_CLK		| PIN_OUTPUT_LOW,
	GPIO196_MC1_CMD		| PIN_INPUT_PULLUP,
	GPIO197_MC1_CMDDIR	| PIN_OUTPUT_HIGH,
	GPIO198_MC1_FBCLK	| PIN_INPUT_NOPULL,
	GPIO199_MC1_DAT0DIR	| PIN_OUTPUT_HIGH,
	/* SD-CARD detect/levelshifter pins */
	GPIO180_GPIO		| PIN_INPUT_PULLUP,
	GPIO227_GPIO,
	GPIO185_GPIO,

	/* SDI3 (SDIO) */
	GPIO171_MC3_DAT0	| PIN_INPUT_PULLUP,
	GPIO172_MC3_DAT1	| PIN_INPUT_PULLUP,
	GPIO173_MC3_DAT2	| PIN_INPUT_PULLUP,
	GPIO174_MC3_DAT3	| PIN_INPUT_PULLUP,
	GPIO175_MC3_CMD		| PIN_INPUT_PULLUP,
	GPIO176_MC3_CLK		| PIN_OUTPUT_LOW,

	/* Display & HDMI HW sync */
	GPIO204_LCD_VSI1	| PIN_INPUT_PULLUP,

	/* Camera & MMIO XshutDown*/
	GPIO1_GPIO		| PIN_OUTPUT_LOW,
	GPIO2_GPIO		| PIN_OUTPUT_LOW,

	/* USB chip select */
	GPIO76_GPIO		| PIN_OUTPUT_LOW,

	GPIO202_ACCU0_RXD	| PIN_INPUT_PULLUP,
	GPIO203_ACCU0_TXD	| PIN_OUTPUT_HIGH,
};

static UX500_PINS(u5500_pins_i2c1,
	GPIO3_I2C1_SCL,
	GPIO4_I2C1_SDA,
);

static UX500_PINS(u5500_pins_i2c2,
	GPIO218_I2C2_SCL,
	GPIO219_I2C2_SDA,
);

static UX500_PINS(u5500_pins_spi3,
	GPIO187_SPI3_CS0n	| PIN_OUTPUT_HIGH,
	GPIO188_SPI3_RXD	| PIN_INPUT_PULLDOWN,
	GPIO189_SPI3_TXD	| PIN_OUTPUT_LOW,
	GPIO190_SPI3_CLK	| PIN_OUTPUT_LOW,
);

static struct ux500_pin_lookup u5500_pins[] = {
	PIN_LOOKUP("nmk-i2c.1", &u5500_pins_i2c1),
	PIN_LOOKUP("nmk-i2c.2", &u5500_pins_i2c2),
	PIN_LOOKUP("spi3", &u5500_pins_spi3),
};

void __init u5500_pins_init(void)
{
	nmk_config_pins(u5500_pins_default, ARRAY_SIZE(u5500_pins_default));
	ux500_pins_add(u5500_pins, ARRAY_SIZE(u5500_pins));
}
