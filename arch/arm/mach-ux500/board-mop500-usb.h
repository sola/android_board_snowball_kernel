/*
 * Copyright (C) ST-Ericsson SA 2011
 *
 * Author: Saketh Ram Bommisetti <sakethram.bommisetti@stericsson.com>
 * License terms: GNU General Public License (GPL) version 2
 */

#ifndef __BOARD_MOP500_USB_H
#define __BOARD_MOP500_USB_H

extern struct ab8500_usbgpio_platform_data ab8500_usbgpio_plat_data;

#endif
